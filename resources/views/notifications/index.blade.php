@extends('layouts.app2')

@section('content')
    <ul class="list-group">
        @foreach($items as $key => $item)
            <li class="list-group-item">{{ $key+1 .'. '.$item->message.' '.$item->humanIsRead }}</li>
        @endforeach
    </ul>
@endsection

