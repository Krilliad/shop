@extends('layouts.app2')
@section('content')
    <div class="container-sm d-flex p-5 justify-between">
        @foreach($items as $item)
            <div class="card" style="width: 18rem;">
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">{{ $item->name }}</h5>
                    <a href="#" class="btn btn-primary">Show</a>
                    @can('subscribe', $item)
                        <a href="{{ route('shops.subscribe', $item) }}" class="btn btn-primary">Subscribe</a>
                    @endcan
                    @can('publish', $item)
                        <a href="{{ route('shops.publish', $item) }}" class="btn btn-primary">Publish</a>
                    @endcan
                </div>
            </div>
        @endforeach
    </div>
@endsection
