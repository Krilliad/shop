<?php

namespace App\Domain\EventChannels;

use App\Domain\EventChannels\Interfaces\SubscriberInterface;
use App\Models\Notification;
use App\Models\Shop;
use App\Models\ShopsSubscribers;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;

class Subscriber implements SubscriberInterface
{
    public User $user;

    public Shop $shop;

    public function __construct(User $user, Shop $shop)
    {
        $this->user = $user;
        $this->shop = $shop;
    }

    /**
     * @throws Exception
     */
    public function subscribe()
    {
        if ($this->subscriberExist()) {
            add_notice('you are already subscribed to this shop', 'warn');
        } else {
            DB::beginTransaction();
            try {
                $this->createShopSubscriber();

                $this->notifySubscriber();

                $this->notifyShopDirector();

                add_notice('success');
                DB::commit();
            } catch (Exception $exception) {
                DB::rollBack();
                add_notice('server error', 'error');
            }
        }
    }

    private function createShopSubscriber()
    {
        ShopsSubscribers::query()->create([
            'shop_id' => $this->shop->id,
            'user_id' => $this->user->id,
        ]);
    }

    private function subscriberExist(): bool
    {
        return ShopsSubscribers::query()
            ->where('user_id', $this->user->id)
            ->where('shop_id', $this->shop->id)
            ->exists();
    }

    private function notifySubscriber()
    {
        Notification::query()->create([
            'user_id' => $this->user->id,
            'message' => 'Вы подписались на ' . $this->shop->name,
            'is_read' => 0,
        ]);
    }

    private function notifyShopDirector()
    {
        Notification::query()->create([
            'user_id' => $this->shop->director->id,
            'message' => 'На ваш магазин подписался ' . $this->user->name,
            'is_read' => 0,
        ]);
    }
}
