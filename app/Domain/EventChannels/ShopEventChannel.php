<?php

namespace App\Domain\EventChannels;

use App\Domain\EventChannels\Interfaces\PublisherInterface;
use App\Domain\EventChannels\Interfaces\ShopEventChannelInterface;
use App\Domain\EventChannels\Interfaces\SubscriberInterface;

class ShopEventChannel implements ShopEventChannelInterface
{
    public function subscribe(SubscriberInterface $subscriber)
    {
        $subscriber->subscribe();
    }

    public function publish(PublisherInterface $publisher)
    {

    }
}
