<?php

namespace App\Domain\EventChannels;

use App\Domain\EventChannels\Interfaces\PublisherInterface;
use App\Domain\EventChannels\Interfaces\ShopEventChannelInterface;
use App\Models\Shop;

class Publisher implements PublisherInterface
{
    private Shop $shop;

    private ShopEventChannelInterface $eventChannel;

    public function __construct(Shop $shop, ShopEventChannelInterface $eventChannel)
    {
        $this->shop = $shop;
        $this->eventChannel = $eventChannel;
    }

    public function publish()
    {
        $this->eventChannel->publish($this->shop);
    }

    public function notify()
    {

    }
}
