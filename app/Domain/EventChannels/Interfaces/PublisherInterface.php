<?php

namespace App\Domain\EventChannels\Interfaces;

use App\Models\Shop;

interface PublisherInterface
{
    public function notify();
}
