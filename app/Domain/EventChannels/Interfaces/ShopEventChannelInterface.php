<?php

namespace App\Domain\EventChannels\Interfaces;

interface ShopEventChannelInterface
{
    public function subscribe(SubscriberInterface $subscriber);

    public function publish(PublisherInterface $publisher);
}
