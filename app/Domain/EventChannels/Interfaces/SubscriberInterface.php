<?php

namespace App\Domain\EventChannels\Interfaces;

interface SubscriberInterface
{
    public function subscribe();
}
