<?php

namespace App\Services;

use App\Domain\EventChannels\Publisher;
use App\Domain\EventChannels\ShopEventChannel;
use App\Domain\EventChannels\Subscriber;
use App\Models\Shop;
use Illuminate\Support\Facades\Auth;

class ShopService
{
    private ShopEventChannel $event;

    public function __construct(ShopEventChannel $event)
    {
        $this->event = $event;
    }

    public function subscribe(Shop $shop)
    {
        $this->event->subscribe(new Subscriber(Auth::user(), $shop));
    }

    public function publish(Shop $shop)
    {
        $this->event->publish($shop, new Publisher($shop, $this->event));
    }
}
