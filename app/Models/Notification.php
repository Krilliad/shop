<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Notification extends Model
{
    protected $fillable = [
        'user_id',
        'message',
        'is_read',
    ];

    public function scopeByUser(Builder $builder, ?User $user = null): Builder
    {
        $user = $user ?? Auth::user();

        return $builder->where('user_id', $user->id);
    }

    public function getHumanIsReadAttribute(): string
    {
        return $this->is_read ? 'Прочитано' : 'Не прочитано';
    }

    public static function getCountNotificationsByUser(?User $user = null): int
    {
        $user = $user ?? Auth::user();

        return self::query()->scopes(['byUser' => $user])->where('is_read', 0)->count();
    }
}
