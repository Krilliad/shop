<?php

namespace App\Models\Contracts;

interface UserRoleContract
{
    public const ROLES = [
        self::USER,
        self::STORE_DIRECTOR,
    ];

    public const
        USER = 'user', // обычный юзер
        STORE_DIRECTOR = 'store_director' // директор магазина
    ;
}
