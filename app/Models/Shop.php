<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Shop extends Model
{
    public function director(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
