<?php

namespace App\Http\Controllers;

use App\Models\Notification;

class NotificationController extends Controller
{
    public function index()
    {
        Notification::query()->scopes('byUser')->where('is_read', 0)->update(['is_read' => 1]);

        $items = Notification::query()->scopes('byUser')->get();

        return view('notifications.index', compact('items'));
    }
}
